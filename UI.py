from database import searchdb
import server
import os
import tkinter as tk
from tkinter import ttk
import json
import time
import join
from threading import Thread
from socket import *
from client import *



root = tk.Tk()

root.title('Inter Port Messaging System')
root.geometry('1000x700+50+100')
root.resizable(False, False)
root.iconbitmap('symbol.ico')


def get_query():
    query = mmsi.get()
    ans = 'Invalid MMSI'
    if len(query) == 9:
        ans = searchdb(query)
        if ans == 'NACK':
            ans = update_wantList(query)
    displaybox.delete("1.0","end")
    ans.replace(':', ':\t\t\t')
    displaybox.insert('1.0', ans)

def close_UI():
    exit()

def join_network():
    ret = join.join_network()
    if  ret != "fail":
        connectedStatus.delete('1.0', 'end')
        print(ret)
        connectedStatus.insert('1.0', "CONNECTED")
    else:
        displaybox.delete("1.0","end")
        connectedStatus.insert('1.0', "UNABLE TO CONNECT")





connectionFrame = tk.Frame(root)
connectionFrame.pack(side='top', expand=False, fill='x')


connect_button = ttk.Button(connectionFrame, text="connect", command=join_network)
connect_button.pack(side='left')
connectedStatus = tk.Text(connectionFrame, height=1, width=20)
connectedStatus.pack(fill='x', expand=False, side='left')
connectedStatus.insert('1.0', "NOT CONNECTED")


display = tk.Frame(root, height=400, width=350)
display.pack(side='bottom', expand=False, pady=100)
displaybox = tk.Text(display)
displaybox.pack(expand=True)

mmsi = tk.StringVar()
search = ttk.Frame(root, height=20, width=100)
search.pack(side = 'top', padx=10,expand=False)
search_button = ttk.Button(search, text="Request Update", command=get_query)
search_button.pack(side='top')

search_label = ttk.Label(search, text="MMSI:")
search_label.pack(side='left')
searchbox = ttk.Entry(search, textvariable=mmsi)
searchbox.pack(side='left')
searchbox.focus()




root.mainloop()





