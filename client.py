from socket import *
import json
from database import API_call


def store(new_ship):
    mmsi = new_ship[0:10]
    shipsFile = open(gethostname() + "_ships.json", 'r+')
    ships = json.load(shipsFile)
    ship_dict = dict()
    ship_dict.update({mmsi: dict()})
    
    lines = new_ship.split('\n')
    for l in lines:
        if len(l) == 0:
            break
        field = l.split(':')
        ship_dict[mmsi].update({field[0]: field[1]})
        
    if mmsi in ships:
        ships[mmsi] = ship_dict
    else:
        ships.update({mmsi:ship_dict})
        
    
    shipsFile.seek(0)
    json.dump(ships, shipsFile, indent=5)
        
    shipsFile.close()
    
    return



def update_wantList(MMSI):	
	

	dataFile = open('nodeData.json', 'r')

	reader = json.load(dataFile)

	peers = reader["peers"]

	msg = ""

	sock = socket(AF_INET, SOCK_STREAM)

	for p in peers:

		sock.connect((p,12345 )) 
		sock.send(MMSI.encode())

		print('updated {}\nadding {} to our own want-list'.format(p, MMSI))
	
		msg = sock.recv(1000).decode()
		sock.close()

		if msg != 'NACK':
			response = msg
			store(response)

			return response
	

	if msg == 'NACK':
		return API_call(MMSI)

###########################################   API?
	else:
    		return "error"


	
	
		





		

def message(addr, msg):

	sock = socket(AF_INET, SOCK_STREAM)


	sock.connect((addr, 12345))


	sock.send(msg.encode())

	ret = sock.recv(1000).decode()

	sock.close()

	return ret