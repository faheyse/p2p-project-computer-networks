from socket import *


def PING(addr):
    sock = socket(AF_INET, SOCK_STREAM)

    sock.connect((addr, 12345))
    sock.send('PING'.encode())
    print('sent PING')
    response = sock.recv(100).decode()

    sock.close()

    return response
