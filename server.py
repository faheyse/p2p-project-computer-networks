# server for computer

from socket import *

import json

from database import searchdb


def run():
    
    sock = socket(AF_INET, SOCK_STREAM)
    sock.bind(('', 12345))    
    sock.listen(5)

    print('server listening on port {}...'.format(12345))

    while 1:
        (connectedSock, addr) = sock.accept()
        msg = connectedSock.recv(100).decode()
        
        if msg == 'New peer request':
            dataFile = open('nodeData.json', 'r+')

            nodeData = json.load(dataFile)
            nodeSet = set(nodeData["peers"])
            nodeSet.add(addr[0])    
            nodeData['peers'] = list(nodeSet)
            dataFile.seek(0)

            json.dump(nodeData,dataFile, indent=5)

            dataFile.close()

            connectedSock.send('Accepted new peer...requesting want-list'.encode())
           
            wlist = connectedSock.recv(200).decode()

            if wlist == chr(6):     # want-list is empty
                connectedSock.close()
                continue

            update = searchdb(wlist)

            if update != 'NACK':
                connectedSock.send(update.encode())

            else:
                dataFile = open('nodeData.json', 'r+')
                
                nodeData = json.load(dataFile)

                for w in wlist:
                    
                    if addr[0] in nodeData["want-lists"]:
                        
                        nodeData["want-lists"][addr[0]].append[w]
                else:
                    nodeData["want-lists"][addr[0]] = [w]


                dataFile.seek(0)

                json.dump(nodeData,dataFile, indent=5)

                dataFile.close()


            connectedSock.close()



        if msg == 'PING':

            connectedSock.send('ACK'.encode())

            print('tracker PING')

            connectedSock.close()
        

        if msg == 'close':

            connectedSock.close()

            return


        elif len(msg) == 9 and msg.isnumeric():
            newShip = searchdb(msg)
            connectedSock.send(newShip.encode())


