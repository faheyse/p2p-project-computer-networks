only 2 files are to be run;
run_tracker.py     -    run on a linux machine (e.g raspberry pi)
run_terminal.py    -    run on any machine with python 3 installed and has a GUI (e.g will run on ubuntu desktop but not ubuntu Ubuntu server)



on the linux machine, simply type $python run_tracker.py

on the other machine, run $python run_terminal.py
you will be prompted to enter the name of a known tracker node. peers cannot join the network unless there is prior knowledge of 
the ip address OR hostname of a tracker. 
e.g:
    Please enter the name of a node running the tracker daemon (either the ip address or the hostname)
    >>>rasp-020
A user interface will then appear. The 'connect' button connects you to the tracker node, who will provide you with a list of peers 
currently active on the network (all of this is automated). The textbox should then read 'CONNECTED' if the tracker was successfully reached.
Now, you can provide an MMSI nuber for a freight ship, and request its current whereabouts and journey information.

Note:
to begin with, this database has no data in it. when you provide an MMSI which is not recognised in the network, an external api to www.marinetraffic.com 
is queried, and is added to the database. 
