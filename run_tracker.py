import json
import os
import socket

# Initailising csv and json files 
file = open('nodeData.json', 'w')
initial = {
     "tracker": "",
     "peers": [
     ],
     "want-lists": {}
}
jf = initial
file.seek(0)
json.dump(jf, file, indent=5)
file.close()
    
list_of_ships = open('ships.csv', 'r')
dataBaseFile = open(socket.gethostname()+'_ships.json', 'w')
topdict = dict()
template =  {
          "MMSI": "",
          "LAT": "",
          "LON": "",
          "SPEED": "",
          "HEADING": "",
          "COURSE": "",
          "STATUS": "",
          "TIMESTAMP": "",
          "SHIPNAME": "",
          "TYPE_NAME": "",
          "AIS_TYPE_SUMMARY": "",
          "IMO": "",
          "CALLSIGN": "",
          "FLAG": "",
          "PORT_ID": "",
          "PORT_UNLOCODE": "",
          "CURRENT_PORT": "",
          "LAST_PORT_ID": "",
          "LAST_PORT_UNLOCODE": "",
          "LAST_PORT": "",
          "LAST_PORT_TIME": "",
          "DESTINATION": "",
          "ETA": "",
          "ETA_CALC": "",
          "LENGTH": "",
          "WIDTH": "",
          "DRAUGHT": "",
          "GRT": "",
          "NEXT_PORT_ID": "",
          "NEXT_PORT_UNLOCODE": "",
          "NEXT_PORT_NAME": "",
          "NEXT_PORT_COUNTRY": "",
          "DWT": "",
          "YEAR_BUILT": "",
          "DSRC": ""
}

for ship in list_of_ships:
    topdict.update({ship:template})

list_of_ships.close()
dataBaseFile.seek(0)
json.dump(topdict, dataBaseFile, indent=5)
dataBaseFile.close()


# this is the tracker daemon
os.system('python tracker.py')