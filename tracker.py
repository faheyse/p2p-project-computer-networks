"""
program to hold IP addresses of peers on the network, and to introduce peers to each other

It should also PING periodically to check if peers are sill connected
"""
from socket import *
import os
import time
import ping
import server
import json


# clear out any data entries from previous sessions...
nodeFile = open('nodeData.json', 'r+')
jfile = json.load(nodeFile)
peers = set(jfile['peers'])
peers.add(gethostbyname(gethostname()))
jfile['peers'] = list(peers)
nodeFile.seek(0)
json.dump(jfile, nodeFile, indent=5)
nodeFile.close()

ConnectedNodes = set(jfile['peers'])

port = 54321
sock = socket(AF_INET, SOCK_STREAM)

sock.bind(('', port))
sock.listen(10)
print('listening...')


pid = os.fork()


if pid > 0 :

    while 1:
        (connectedSock, addr) = sock.accept()
        print('addr: {}'.format(addr[0]))
        msg = connectedSock.recv(300).decode()
        print('received message: '+ msg)
        if msg == "JOIN":
            ConnectedNodes.add(addr[0])
        else:
            connectedSock.close()
            exit()

        msg = 'Accepted request to join the network. Sending IP addresses of connected nodes...'
        connectedSock.send(msg.encode())
    
        msg = ""
        for n in ConnectedNodes:
            if n == addr[0]:
                continue
            msg += (n + " ")

        connectedSock.send(msg.encode())
        print(msg)
        connectedSock.close()

else:
    pid2 = os.fork()

    if pid2 >0:
        otherNodes = ConnectedNodes
        otherNodes.remove(gethostbyname(gethostname()))
        while 1:
            time.sleep(60*3)
            for i in otherNodes:
                if ping.PING(i) != 'ACK':
                    ConnectedNodes.remove(i)
    
    else:
        server.run()
        
