from socket import *
import os
import json


def join_network():
    dataFile = open('nodeData.json', 'r+')
    nodeData = json.load(dataFile)

    addr = nodeData["tracker"]
    print(addr)
    sock = socket(AF_INET, SOCK_STREAM)
    try:
        sock.connect((addr, 54321 ))
    except:
        print('ERROR:   tracker node not found\nexiting')
        sock.close()
        return 'fail'

    print('connected')
    ret = "connected"
    msg = "JOIN"
    sock.send(msg.encode())
    msg = sock.recv(200).decode()
    print(msg)
    if msg != "Accepted request to join the network. Sending IP addresses of connected nodes...":
        print('Error: Could not connect to tracker node')
        ret = "fail"
        exit()
    else:
        print('connected to tracker node on network')
        
        msg = sock.recv(1000).decode().split()
        print(msg)
        peers = set(nodeData["peers"])
        for i in msg:
            peers.add(i)
        peers = list(peers)
        nodeData["peers"] = peers
        dataFile.seek(0)
        json.dump(nodeData, dataFile, indent=5)
        dataFile.close()

    

    sock.close()


    ret = "fail"
# contact peers to say you have joined the network
    for i in peers:
        sock = socket(AF_INET, SOCK_STREAM) 
        sock.connect((i,12345 ))
        print('connected')
        msg = "New peer request"
        sock.send(msg.encode())
	
        msg = sock.recv(200).decode()

        if msg != 'Accepted new peer...requesting want-list':
            peers.remove(i)
        else:
            dataFile = open('nodeData.json', 'r+')
            nodeData = json.load(dataFile)
            try:
                wantList = nodeData["want-lists"][i]
                sock.send(wantList)
            except:
                sock.send(chr(6).encode())

            dataFile.close()
            ret = "connected to peer"

        sock.close()
    


    return ret

