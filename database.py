from time import sleep
import json
import subprocess
from socket import *



def searchdb(request):
    myNode = gethostname()
    shipsFile = open(myNode + "_ships.json", 'r')
    ships = json.load(shipsFile)
    response = ""
    if request in ships:
        for i in ships[request]:
            field = ships[request][i]
            if field == None:
                field = ""
            response += (i + ':' + field + "\n")
        shipsFile.close()
        return response

    # MMSI not found
    # ask peers by adding it to their want-list
    shipsFile.close()
    return 'NACK'





def API_call(mmsi):
    url =  '\"https://services.marinetraffic.com/api/exportvessel/0aefb187af784da87f05041d6ba4c7a6f2b9e404?v=5&protocol=json&timespan=200&msgtype=extended&mmsi='+str(mmsi) + '\"'
    cmd = "curl " + url + " -o tmp_file.json"
    print(cmd)
    subprocess.call(cmd)
    tmpFile = open('tmp_file.json', 'r')
    jtmp = json.load(tmpFile)
    response = jtmp[0]
    print(response)
    new_entry = {mmsi: dict()}
    template = [
    "MMSI",
    "LAT",
    "LON",
    "SPEED",
    "HEADING",
    "COURSE",
    "STATUS",
    "TIMESTAMP",
    "SHIPNAME",
    "SHIPTYPE",
    "TYPE_NAME",
    "AIS_TYPE_SUMMARY",
    "IMO",
    "CALLSIGN",
    "FLAG",
    "PORT_ID",
    "PORT_UNLOCODE",
    "CURRENT_PORT",
    "LAST_PORT_ID",
    "LAST_PORT_UNLOCODE",
    "LAST_PORT",    
    "LAST_PORT_TIME",
    "DESTINATION",
    "ETA",
    "ETA_CALC",
    "LENGTH",
    "WIDTH",
    "DRAUGHT",
    "GRT",
    "NEXT_PORT_ID",
    "NEXT_PORT_UNLOCODE",
    "NEXT_PORT_NAME",
    "NEXT_PORT_COUNTRY",
    "DWT",
    "YEAR_BUILT",
    "DSRC"
    ]
    for i in range(len(template)):
        new_entry[mmsi].update({template[i]:response[i]})
    
    myNode = gethostname()
    shipsFile = open(myNode + "_ships.json", 'r+')
    ships = json.load(shipsFile)
    if mmsi in ships:
        ships[mmsi] = new_entry[mmsi]
    else:
        ships.update(new_entry)
    
    shipsFile.seek(0)
    json.dump(ships, shipsFile, indent=5)
    shipsFile.close()
    
    ans  =""
    for i in new_entry[mmsi]:
        field = new_entry[mmsi][i]
        if field == None:
            field = ""
        ans += (i + ':' + field + "\n")
    
    return ans
